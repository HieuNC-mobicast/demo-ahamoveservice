package com.example.demo.constant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AhamoveConstant {
	public static final String AHAMOVE_API_KEY = "4136ea40d44ce70815800364a58fe7c8";
	public static final String PHONE = "84908842280";
	public static final String ACCOUNT = "Ahamove Test Create+User";
    public static final String ADDRESS = "177 Trung Kinh, Home City";
}