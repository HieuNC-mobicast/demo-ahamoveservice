package com.example.demo.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;

import com.example.demo.dto.ApiError;
import com.example.demo.dto.Response;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.Setter;

public class RestTemplateService {

	@Getter
    @Setter
    private static ObjectMapper mapper = new ObjectMapper();

    public static <T> ResponseEntity<Response<T>> getEntity(String token, String url, Class<T> responseType, Map<String, String> uriVariables) throws URISyntaxException {
        RestTemplate restTemplate = createRestTemplate();
        return uriVariables == null ? restTemplate.execute(new URI(url), HttpMethod.GET, createRequest(token), getEntityExtractor(responseType))
                : restTemplate.execute(url, HttpMethod.GET, createRequest(token), getEntityExtractor(responseType), uriVariables);
    }

    public static <T> ResponseEntity<Response<List<T>>> getListEntity(String token, String url, Class<T> responseType, Map<String, String> uriVariables) throws URISyntaxException {
        RestTemplate restTemplate = createRestTemplate();
        return uriVariables == null ? restTemplate.execute(new URI(url), HttpMethod.GET, createRequest(token), getListEntityExtractor(responseType))
                : restTemplate.execute(url, HttpMethod.GET, createRequest(token), getListEntityExtractor(responseType), uriVariables);
    }

    public static <T> ResponseEntity<Response<List<T>>> getListFromPageEntity(String token, String url, Class<T> responseType, Map<String, String> uriVariables) throws URISyntaxException {
        RestTemplate restTemplate = createRestTemplate();
        return uriVariables == null ? restTemplate.execute(new URI(url), HttpMethod.GET, createRequest(token), getListEntityFromPageExtractor(responseType))
                : restTemplate.execute(url, HttpMethod.GET, createRequest(token), getListEntityFromPageExtractor(responseType), uriVariables);
    }

    public static <T> ResponseEntity<Response<T>> postEntity(String token, String url, Object requestBody, Class<T> responseType, Map<String, String> uriVariables) throws URISyntaxException {
        RestTemplate restTemplate = createRestTemplate();
        return uriVariables == null ? restTemplate.execute(new URI(url), HttpMethod.POST, createPostRequest(token, requestBody, restTemplate), getEntityExtractor(responseType))
                : restTemplate.execute(url, HttpMethod.POST, createPostRequest(token, requestBody, restTemplate), getEntityExtractor(responseType), uriVariables);
    }

    public static <T> ResponseEntity<Response<List<T>>> postForListEntity(String token, String url, Object requestBody, Class<T> responseType, Map<String, String> uriVariables) throws URISyntaxException {
        RestTemplate restTemplate = createRestTemplate();
        return uriVariables == null ? restTemplate.execute(new URI(url), HttpMethod.POST, createPostRequest(token, requestBody, restTemplate), getListEntityExtractor(responseType))
                : restTemplate.execute(url, HttpMethod.POST, createPostRequest(token, requestBody, restTemplate), getListEntityExtractor(responseType), uriVariables);
    }

    public static <T> ResponseEntity<Response<List<T>>> postListFromPageEntity(String token, Object requestBody, String url, Class<T> responseType, Map<String, String> uriVariables) throws URISyntaxException {
        RestTemplate restTemplate = createRestTemplate();
        return uriVariables == null ? restTemplate.execute(new URI(url), HttpMethod.POST, createPostRequest(token, requestBody, restTemplate), getListEntityFromPageExtractor(responseType))
                : restTemplate.execute(url, HttpMethod.POST, createPostRequest(token, requestBody, restTemplate), getListEntityFromPageExtractor(responseType), uriVariables);
    }

    public static <T> ResponseEntity<Response<T>> putEntity(String token, String url, Object requestBody, Class<T> responseType, Map<String, String> uriVariables) throws URISyntaxException {
        RestTemplate restTemplate = createRestTemplate();
        return uriVariables == null ? restTemplate.execute(new URI(url), HttpMethod.PUT, createPostRequest(token, requestBody, restTemplate), getEntityExtractor(responseType))
                : restTemplate.execute(url, HttpMethod.PUT, createPostRequest(token, requestBody, restTemplate), getEntityExtractor(responseType), uriVariables);
    }

    public static <T> ResponseEntity<Response<T>> deleteEntity(String token, String url, Class<T> responseType, Map<String, String> uriVariables) throws URISyntaxException {
        RestTemplate restTemplate = createRestTemplate();
        return uriVariables == null ? restTemplate.execute(new URI(url), HttpMethod.DELETE, createRequest(token), getEntityExtractor(responseType))
                : restTemplate.execute(url, HttpMethod.GET, createRequest(token), getEntityExtractor(responseType), uriVariables);
    }

    @SuppressWarnings("unchecked")
    private static <T> ResponseExtractor<ResponseEntity<Response<T>>> getEntityExtractor(Class<T> responseType) {
        return response -> {
            ObjectMapper mapper = new ObjectMapper();
            HttpHeaders headers = response.getHeaders();
            HttpStatus status = response.getStatusCode();
            JsonNode root = mapper.readTree(response.getBody());
            Response res = createResponse(root);
            if (root.get("body").isNull()) {
                res.setBody(null);
            } else {
                res.setBody(getMapper().convertValue(root.get("body"), responseType));
            }
            return new ResponseEntity<>(res, headers, status);
        };
    }

    private static <T> ResponseExtractor<ResponseEntity<Response<List<T>>>> getListEntityExtractor(Class<T> responseType) {
        return response -> {
            ObjectMapper mapper = new ObjectMapper();
            HttpHeaders headers = response.getHeaders();
            HttpStatus status = response.getStatusCode();
            JsonNode root = mapper.readTree(response.getBody());
            Response<List<T>> res = createResponse(root);
            List<T> list = mapper.convertValue(root.get("body"), mapper.getTypeFactory().constructCollectionType(List.class, responseType));
            res.setBody(list);
            return new ResponseEntity<>(res, headers, status);
        };
    }

    private static <T> ResponseExtractor<ResponseEntity<Response<List<T>>>> getListEntityFromPageExtractor(Class<T> responseType) {
        return response -> {
            ObjectMapper mapper = new ObjectMapper();
            HttpHeaders headers = response.getHeaders();
            HttpStatus status = response.getStatusCode();
            JsonNode root = mapper.readTree(response.getBody());
            Response<List<T>> res = createResponse(root);
            List<T> list = mapper.convertValue(root.get("body").get("content"), mapper.getTypeFactory().constructCollectionType(List.class, responseType));
            res.setBody(list);
            return new ResponseEntity<>(res, headers, status);
        };
    }

    private static ApiError createApiError(JsonNode root) {
        JsonNode errorNode = root.get("error");
        ApiError error = new ApiError();
        error.setSystemCode(errorNode.get("systemCode").asText(null));
        error.setMessage(errorNode.get("message").asText(null));
        error.setTrace(errorNode.get("trace").asText(null));
        error.setObject(errorNode.get("object").asText(null));
        JsonNode fields = errorNode.get("fields");
        if (fields.isArray()) {
            for (int i = 0, j = fields.size(); i < j; i++) {
                JsonNode field = fields.get(i);
                error.addValidationError(null, field.get("field").textValue(), null, field.get("apiErrorCode").textValue(), field.get("message").textValue());
            }
        }
        return error;
    }

    private static ApiError setApiError(JsonNode root) {
        if (root.get("status").textValue().equals("ERROR")) {
            return createApiError(root);
        } else return null;
    }

    private static <T> Response<T> createResponse(JsonNode root) {
        Response<T> response = new Response<>();
        response.setBody(null);
        response.setError(setApiError(root));
        response.setStatus(root.get("status").asText());
        return response;
    }

    private static RequestCallback createRequest(String token) {
        return request -> {
            HttpHeaders headers = request.getHeaders();
            headers.add("Authorization", token);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
        };
    }

    private static HttpEntity<?> createHttpEntity(String token, Object body) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", token);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(body, headers);
    }

    private static RequestCallback createPostRequest(String token, Object requestBody, RestTemplate restTemplate) {
        return restTemplate.httpEntityCallback(createHttpEntity(token, requestBody));
    }

    private static RestTemplate createRestTemplate() {
        return new RestTemplate(); // preconfig for restTemplate here
    }
}
