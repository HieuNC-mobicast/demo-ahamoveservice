package com.example.demo.service;

import java.net.URISyntaxException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.demo.constant.AhamoveConstant;
import com.example.demo.dto.AhamoveToken;
import com.example.demo.dto.OrderBody;

@Service
public class AhamoveService {
    public RestTemplate getRestTemplate() {
    	RestTemplate connector = new RestTemplate();
    	return connector;
    }
    public String getApiKey() {
    	return AhamoveConstant.AHAMOVE_API_KEY;
    }
    public String getPhoneNumber() {
    	return AhamoveConstant.PHONE;
    }
    public String getAccountName() {
    	return AhamoveConstant.ACCOUNT;
    }
    public String getAddress() {
    	return AhamoveConstant.ADDRESS;
    }
    public AhamoveToken getToken() {
    	RestTemplate connector = this.getRestTemplate();
    	HttpHeaders headers = new HttpHeaders();
    	headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
    	String httpUrl = "https://apistg.ahamove.com/v1/partner/register_account";
    	UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(httpUrl)
    			.queryParam("api_key", this.getApiKey())
    			.queryParam("name", this.getAccountName())
    			.queryParam("mobile", this.getPhoneNumber())
    			.queryParam("address", this.getAddress());
    	ResponseEntity<AhamoveToken> response = connector.getForEntity(builder.toUriString(), AhamoveToken.class);
    	return response.getBody();
    }
    public Object createOrder(OrderBody orderBody, Class responseType) throws URISyntaxException {
    	return (Object)RestTemplateService.postEntity(null, "https://apistg.ahamove.com/v1/order/create", orderBody, responseType, null);
    }
}
