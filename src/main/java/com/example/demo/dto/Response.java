package com.example.demo.dto;

import lombok.Getter;
import lombok.Setter;

public class Response<T> {
    private final static String OK = "OK";
    private final static String ERROR = "ERROR";
    @Getter @Setter
    private String status;
    
    @Getter @Setter
    private ApiError error;
    
    @Getter @Setter
    private T body;
    
    public Response(){
        this.status = OK;
        this.error = null;
        this.body = null;
    }
    public Response (T body){
        this();
        this.body = body;
    }
    public Response (ApiError error){
        this.status = ERROR;
        this.error = error;
        this.body = null;
    }
    public Response (ApiError error, T body){
        this(body);
        this.error = error;
    }
    public Response (String status, ApiError error, T body){
        this(error,body);
        this.status = status;
    }
}
