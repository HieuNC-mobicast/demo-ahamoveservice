package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class Location {
	private double lat;
	private double lng;
	private String name;
	private String address;
	private String recipientMobile;
	private String remark;
	private String shortAddress;	
}
