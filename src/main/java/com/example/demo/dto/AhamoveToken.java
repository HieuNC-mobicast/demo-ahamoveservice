package com.example.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AhamoveToken {
	private String token;
	private String refresh_token;
}
