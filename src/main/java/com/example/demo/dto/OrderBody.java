package com.example.demo.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderBody {
	
	private double orderTime;
	private List<Location> path;
	private String serviceId;
	private String paymentMethod;
	private List<Item> items;
	private double idleUtil;
}
