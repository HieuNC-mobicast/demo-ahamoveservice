package com.example.demo.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

public class ApiError {

	@Getter @Setter
    private String systemCode;

    @Getter @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;

    @Getter @Setter
    private String message;

    @Getter @Setter
    private String trace;

    @Getter @Setter
    private List<ApiSubError> fields;
    
    @Getter @Setter
    private String object;

    public ApiError() {
        timestamp = LocalDateTime.now();
    }

    public ApiError(String systemCode) {
        this();
        this.systemCode = systemCode;
    }

    public ApiError(String status, Throwable ex) {
        this();
        this.systemCode = status;
        this.message = "Unexpected error";
        this.trace = ex.getLocalizedMessage();
    }

    public ApiError(String status, String message, Throwable ex) {
        this();
        this.systemCode = status;
        this.message = message;
        this.trace = ex.getLocalizedMessage();
    }

    public void addSubError(ApiSubError subError) {
        if (fields == null) {
            fields = new ArrayList<>();
        }
        fields.add(subError);
    }

    public void addValidationError(String object, String field, Object rejectedValue, String message) {
        addSubError(new ApiValidationError(object, field, rejectedValue, message));
    }

    public void addValidationError(String object, String field, Object rejectedValue, String errorCode, String message) {
        addSubError(new ApiValidationError(object, field, rejectedValue, errorCode, message));
    }

    public void addValidationError(String object, String message) {
        addSubError(new ApiValidationError(object, message));
    }

//    public void addValidationError(FieldError fieldError) {
//        this.addValidationError(
//                fieldError.getObjectName(),
//                fieldError.getField(),
//                fieldError.getRejectedValue(),
//                fieldError.getCode(),
//                fieldError.getDefaultMessage());
//    }

//    public void addValidationErrors(List<FieldError> fieldErrors) {
//        fieldErrors.forEach(this::addValidationError);
//    }
//
//    public void addValidationError(ObjectError objectError) {
//        this.addValidationError(
//                objectError.getObjectName(),
//                objectError.getDefaultMessage());
//    }
//
//    public void addValidationError(List<ObjectError> globalErrors) {
//        globalErrors.forEach(this::addValidationError);
//    }

    /**
     * Utility method for adding error of ConstraintViolation. Usually when a @Validated validation fails.
     *
     * @param cv the ConstraintViolation
     */
//    public void addValidationError(ConstraintViolation<?> cv) {
//        this.addValidationError(
//                cv.getRootBeanClass().getSimpleName(),
//                ((PathImpl) cv.getPropertyPath()).getLeafNode().asString(),
//                cv.getInvalidValue(),
//                cv.getMessage());
//    }
//
//    void addValidationErrors(Set<ConstraintViolation<?>> constraintViolations) {
//        constraintViolations.forEach(this::addValidationError);
//    }


    public abstract class ApiSubError {

    }

    @EqualsAndHashCode(callSuper = false)
    @AllArgsConstructor
    public class ApiValidationError extends ApiSubError {

        @Getter @Setter
        private String object;

        @Getter @Setter
        private String field;

        @Getter @Setter
        private Object rejectedValue;

        @Getter @Setter
        private String apiErrorCode;

        @Getter @Setter
        private String message;

        public ApiValidationError(String object, String message) {
            this.object = object;
            this.message = message;
        }

        public ApiValidationError(String object, String field, Object rejectedValue, String message) {
            this.object = object;
            this.field = field;
            this.rejectedValue = rejectedValue;
            this.message = message;
        }
    }
}
