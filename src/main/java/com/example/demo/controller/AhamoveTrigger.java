package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.AhamoveToken;
import com.example.demo.service.AhamoveService;

@RestController
@RequestMapping("/ahamove")
public class AhamoveTrigger {
	
	@Autowired
	private AhamoveService ahamoveService;
	
	@Autowired
	private AhamoveToken token;
	
	@RequestMapping("/test")
	public String getAhamoveService() {
		
		return "test";
	}
}
